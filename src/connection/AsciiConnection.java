package connection;

import java.nio.charset.StandardCharsets;

public class AsciiConnection extends DeviceConnectionJSerialCom<String, String>{


    public AsciiConnection(String portName) {
        super(portName, true);
    }

    @Override
    public String sendMessage(String msgObject) {
        String response = null;
        byte[] res;
        try {
            discardIncomingBytes();

            // Write message to serial port
            byte[] msg = msgObject.getBytes();
            for(int i=0; i < msg.length; i++) {
                port.writeBytes(new byte[]{msg[i]}, 1);
                Thread.sleep(10);
            }
            logger.trace("Wrote request message to serial port {}", port.getSystemPortName());

            final byte[] inBuf = new byte[uartInputBufferSize];
            // Read UART_INPUT_BUFFER_SIZE from serial port
            final int readBytes = port.readBytes(inBuf, uartInputBufferSize);
            logger.trace("read {} bytes", readBytes);
            response = new String(inBuf);

        } catch (Exception e) {
            logger.error("Sending message failed.", e);

        }
        return response;
    }

    @Override
    protected void checkUuid(int uuid, String unused) {

    }

    @Override
    protected void parseAck(String s, String unused) {

    }

    @Override
    protected void parseMcuErrorCode(String unused) {

    }


}
