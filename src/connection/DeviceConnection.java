package connection;

public interface DeviceConnection<Request, Response> extends AutoCloseable{
    boolean open();

    @Override
    void close();

    String getPortName();

    Response sendMessage(Request msg);
}
