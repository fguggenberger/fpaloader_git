package connection;

import com.fazecast.jSerialComm.SerialPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.fazecast.jSerialComm.SerialPort.*;

public abstract class DeviceConnectionJSerialCom<Request, Response> implements DeviceConnection<Request, Response> {
    protected final static int TIMEOUT_MILLISECONDS = 3000;    // Serial read timeout in ms
    protected final static int DEFAULT_UART_OUTPUT_BUFFER_SIZE = 4096;//8192;    // Needs to be 4096 for windows/linux,
    // 256 for mac
    protected final static int DEFAULT_UART_INPUT_BUFFER_SIZE = 4096; // Needs to correspond to output buffer size of µC
    protected final Logger logger = LoggerFactory.getLogger(DeviceConnectionJSerialCom.class);
    protected final SerialPort port;
    protected final int uartOutputBufferSize;
    protected final int uartInputBufferSize;
    protected boolean autoConnecting = false;
    protected String portName;

    public DeviceConnectionJSerialCom(final String portName) {
        this(portName, false);
    }

    public DeviceConnectionJSerialCom(final String portName, final boolean autoConnecting) {
        this(portName, autoConnecting, DEFAULT_UART_INPUT_BUFFER_SIZE, DEFAULT_UART_OUTPUT_BUFFER_SIZE);
    }

    public DeviceConnectionJSerialCom(final String portName, final boolean autoConnecting, final int inputBufferSize, final int
            outputBufferSize) {
        logger.info("DeviceConnectionJSerialComm(portName={}, autoConnecting={}, inputBufferSize={}, outputBufferSize={})",
                portName, autoConnecting, inputBufferSize, outputBufferSize);

        this.portName = portName;
        this.uartInputBufferSize = inputBufferSize;
        this.uartOutputBufferSize = outputBufferSize;
        this.autoConnecting = autoConnecting;

        logger.info("Trying SerialPort.getCommPort({})", this.portName);
        port = getCommPort(this.portName);
        port.setComPortTimeouts(TIMEOUT_READ_BLOCKING | TIMEOUT_WRITE_BLOCKING, TIMEOUT_MILLISECONDS,
                TIMEOUT_MILLISECONDS);
        port.setBaudRate(115200);

        if (autoConnecting) {
            open();
        }
    }

    public synchronized boolean open() {
        logger.info("open()");

        if (port.isOpen()) {
            logger.warn("Tried to open already opened port");
            return false;
        }
        final boolean openSuccess = port.openPort();
        if (openSuccess)
            logger.info("Opened port {}", port.getSystemPortName());
        else
            logger.error("Failed to open port {}", portName);
        return openSuccess;
    }

    @Override
    public synchronized void close() {
        logger.info("close");

        if (!port.isOpen()) {
            logger.warn("Tried to close already closed port");
            return;
        }

        logger.info("Closing port {}", port.getSystemPortName());
        port.closePort();
        // Sometimes closing hangs, the next line is therefore useful
        logger.info("Closed port {}", port.getSystemPortName());
    }

    @Override
    public String getPortName() {
        return portName;
    }

    @Override
    public abstract Response sendMessage(final Request msgObject);


    protected boolean discardIncomingBytes() {
        if (!port.isOpen()) {
            logger.warn("Tried operation on closed serial port");
            return false;
        }
        // Discard everything UAB might send to us before we ask it
        if (port.bytesAvailable() == 0) {
            return false;
        }
        while (port.bytesAvailable() > 0) {
            final int bytesAvailable = port.bytesAvailable();
            logger.warn("Discarding {} bytes in input buffer", bytesAvailable);
            port.readBytes(new byte[bytesAvailable], bytesAvailable);
        }

        return true;
    }

    protected void reconnect() {
//        try {
        port.closePort();
        port.openPort();
//        } catch (SerialPortException e) {
//            e.printStackTrace();
//        }
    }


    protected abstract void checkUuid(final int uuid, final Response response);

    protected abstract void parseAck(final Request request, final Response response);

    protected abstract void parseMcuErrorCode(final Response response);
}

