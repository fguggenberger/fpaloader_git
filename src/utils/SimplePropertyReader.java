package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class SimplePropertyReader {
    private static final Logger logger = LoggerFactory.getLogger(SimplePropertyReader.class);

    private String propertiesFile;

    public SimplePropertyReader(final String propertiesFile) {
        this.propertiesFile = propertiesFile;
    }

    public String getProperty(final String key) {
        final Properties properties = new Properties();
        String value=null;

        try (FileReader in = new FileReader(propertiesFile)) {
            properties.load(in);

            logger.info("Properties key = {}", key);
            value = properties.getProperty(key);
            logger.info("Properties value = {}", value);

        } catch (IOException e) {
            logger.warn("Could not find {}", propertiesFile);
        }
        return value;
    }

    public int getPropertyAsInteger(final String key) {
        return Integer.valueOf(getProperty(key));
    }
}
