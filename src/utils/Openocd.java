package utils;


import javafx.concurrent.Task;
import openocd.InterfaceWrapper;
import openocd.OpenOCDWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class Openocd {
    final static Logger logger = LoggerFactory.getLogger(Openocd.class);

    final static String OPENOCD_CONFIG = "/home/grabner/fpa.config";

    final static String address = "localhost";
    final static int port = 6667;

    public static void startOpenocd() {
        try {
            final String[] cmd = new String[]{"openocd", "-f", OPENOCD_CONFIG, "-s", "/home/grabner/"};

            final ProcessBuilder pb = new ProcessBuilder(cmd)
                    .redirectOutput(ProcessBuilder.Redirect.to(new File("openocd.out")))
                    .redirectError(ProcessBuilder.Redirect.to(new File("openocd.err")));

            pb.start();

        } catch (Exception e) {
            logger.error("Failed to start openocd", e);
        }
    }

    public static boolean connect() throws IOException {
        logger.debug("Enter connect()");
        try (final OpenOCDWrapper ocd = new OpenOCDWrapper(new InterfaceWrapper(address, port))) {
            logger.debug("Asking for debug level");
            if(ocd.GENERAL.getDebugLevel() == 3)
                return true;
        } catch (Exception e) {
            logger.error("Error trying connect to openocd", e);
            throw e;
        }
        logger.debug("Leaving connect()");
        return false;
    }

    public static Task getFlashFirmwareTask(String binfile) throws Exception{
        return new Task() {
            @Override
            protected Object call() throws Exception {
                try (final OpenOCDWrapper ocd = new OpenOCDWrapper(new InterfaceWrapper(address, port))) {
                    ocd.GENERAL.halt();
                    updateMessage("clear protection");
                    ocd.FLASH.clearProtection(0);
                    updateMessage("flashing binfile");
                    ocd.FLASH.program(binfile);
                    updateMessage("verifying image");
                    ocd.GENERAL.verifyImage(binfile);
                    updateMessage("set protection");
                    ocd.FLASH.setProtection(0);
                    updateMessage("set boot mode");
                    ocd.TARGET.at91sam4_setBootModeFlash();
                    ocd.GENERAL.reset();
                    //return true;
                }
                return null;
            }
        };

        //return false;
    }

    public static void shutdown() {
        try (final OpenOCDWrapper ocd = new OpenOCDWrapper(new InterfaceWrapper(address, port))) {
            ocd.GENERAL.shutdownServer();
        } catch (Exception e) {
            logger.error("Error while shutting down openocd server", e);
        }
    }
}
