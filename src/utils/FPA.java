package utils;

import connection.AsciiConnection;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FPA {
    static final Logger logger = LoggerFactory.getLogger(FPA.class);
    final static String FIRMWARE_FILE = "/home/grabner/fpa_autosampler.bin";
    private boolean isLsusb;
    private boolean isFtdi;
    private boolean isOcd;
    private boolean isConfig;
    private Device device;
    private boolean isComConnectionPossible;

    public static Task<FPA> getInitTask() {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                String msg = "Checking preconditions...";
                updateMessage(msg);

                FPA fpa = new FPA();
                fpa.checkPrerequisites();

                msg = "Check for latest updates...";
                FileHelper.checkForFirmwareUpdate();

                msg = "Looking for devices...";
                updateMessage(msg);
                fpa.device = FTDI.find();
                logger.info("Found: {}", fpa.device);

                msg = "Connecting device...";
                updateMessage(msg);
                fpa.tryConnectCom();

                return fpa;
            }
        };
    }

    public static Task<Boolean> getCalibrationTask() {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                String msg = "Checking preconditions...";
                updateMessage(msg);

                FPA fpa = new FPA();
                fpa.checkPrerequisites();

                msg = "Looking for devices...";
                updateMessage(msg);
                fpa.device = FTDI.find();
                logger.info("Found: {}", fpa.device);

                msg = "Looking for devices...";
                updateMessage(msg);
                fpa.tryConnectCom();

                if(fpa.isCalibrationPossible()) {
                    msg = "Preparing...";
                    updateMessage(msg);
                    fpa.calibrate();
                    long now = System.currentTimeMillis();
                    long timeout = now + 160 * 1000;
                    int i = 0;
                    while ((now = System.currentTimeMillis()) <= timeout){
                        updateProgress(i, 160);
                        if(i<42){
                            updateMessage("Adjusting Turntable...");
                        } else if (i<80)
                            updateMessage("Calibrate Lift...");
                        else if (i<120)
                            updateMessage("Determine Offset...");
                        else if (i<140)
                            updateMessage("Calibrate Clamp...");
                        else
                            updateMessage("Finishing...");
                        Thread.sleep(1000);
                        i++;
                    }
                }
                else
                    throw new Exception("Not able to start calibration");

                return fpa;
            }
        };
    }

    public static Task<Boolean> getFlashFirmwareTask() {
        Task<Boolean> me = new Task() {
            @Override
            protected Object call() throws Exception {
                FPA fpa = new FPA();
                String msg = "Looking for devices...";
                updateMessage(msg);
                fpa.device = FTDI.find();
                if(fpa.isFirmwareUpdatePossible()){
                    msg = "Starting openocd...";
                    updateMessage(msg);
                    Openocd.startOpenocd();

                    logger.info("Going to sleep...");
                    Thread.sleep(4000);
                    //logger.info("Trying to connect to openocd server");
                    //Openocd.connect();

                    msg = "Flashing binary...";
                    updateMessage(msg);
                    Task t = Openocd.getFlashFirmwareTask(FIRMWARE_FILE);
                    t.messageProperty().addListener(new ChangeListener<String>() {
                        @Override
                        public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                            updateMessage(s);
                        }
                    });
                    Thread th = new Thread(t);
                    th.start();
                    th.join();

                    Openocd.shutdown();
                    return true;
                }
                return false;
            }
        };
        return me;
    }

    private void checkPrerequisites() throws Exception{
        this.isLsusb = isLsubAvailable();
        this.isFtdi = isFtdiEepromAvailable();
        this.isOcd = isOpenocdAvailable();
        this.isConfig = isConfigAvailable();

        if(!isLsusb)
            throw new Exception("lsusb not be found in correct version");
        if(!isFtdi)
            throw new Exception("ftdi-eeprom not found!\nTry to install: sudo apt install ftdi-eeprom");
        if(!isOcd)
            throw new Exception("openocd not found!\n Try to install: sudo apt install openocd");
        if(!isConfig)
            provideConfigs();
    }

    private void provideConfigs() throws Exception {
        logger.info("providing configs...");

        ClassLoader cldr = this.getClass().getClassLoader();
        InputStream inputStream = cldr.getResourceAsStream("config/ftdi.properties");
        FileHelper.copyInputStreamToFile(inputStream, new File(FTDI.CONFIG_FILE));

        inputStream = cldr.getResourceAsStream("config/fpa.config");
        FileHelper.copyInputStreamToFile(inputStream, new File(Openocd.OPENOCD_CONFIG));

        inputStream = cldr.getResourceAsStream("config/fpa_autosampler.0.2.2.bin");
        FileHelper.copyInputStreamToFile(inputStream, new File(FIRMWARE_FILE));
    }



    public void calibrate(){
        String port = findCmdInterface();
        if(port != null) {
            try (AsciiConnection con = new AsciiConnection(port)) {
                con.sendMessage("cal_init_pos_tt\r");
            }
        }else{
            logger.error("No cmd line interface found!");
        }

    }

    private String findCmdInterface() {
        //search for cmd interface
        /***
         * ..../usb1/1-2/1-2.1/1-2.1:1.1/ttyUSB0/
         * ..../usb1/1-2/1-2.1/1-2.1:1.2/ttyUSB1/
         * ..../usb1/1-2/1-2.1/1-2.1:1.3/ttyUSB2/
         * or
         * ..../usb1/1-2/1-2:1.0/ttyUSB0
         * ..../usb1/1-2/1-2:1.1/ttyUSB1
         * ..../usb1/1-2/1-2:1.2/ttyUSB2
         * ..../usb1/1-2/1-2:1.3/ttyUSB3
         */
        String pattern = "...:[0-9].([0-9])\\/(ttyUSB[0-9])";
        //String pattern = "[0-9]-[0-9]\\/[0-9]-[0-9].[0-9]\\/([0-9]-[0-9].[0-9]:[0-9].[0-9])";
        Pattern r = Pattern.compile(pattern);

        try {
            final String[] cmd = new String[]{"ls", "-l", "/sys/bus/usb-serial/devices"};

            final ProcessBuilder pb = new ProcessBuilder(cmd)
                    .redirectError(ProcessBuilder.Redirect.to(new File("findCmdInterf.err")));

            final Process executeProcess = pb.start();

            BufferedReader stdIn = new BufferedReader(new InputStreamReader(executeProcess.getInputStream()));
            String line;
            while ((line = stdIn.readLine()) != null) {
                logger.info(line);
                Matcher m = r.matcher(line);
                if (m.find( )) {
                    if(m.groupCount() == 2){
                        if(Integer.parseInt(m.group(1)) == 2){ //this is the cmd interface
                            logger.info("Found com port {}", m.group(2));
                            return "/dev/" + m.group(2);
                        }
                    }
                }

            }
        } catch (Exception e) {
            logger.error("Failed to find command interface!", e);
        }

        return null;
    }

    private boolean isLsubAvailable(){
        try {
            final String[] cmd = new String[]{"lsusb", "--version"};

            final ProcessBuilder pb = new ProcessBuilder(cmd);
            final Process executeProcess = pb.start();

            BufferedReader stdIn = new BufferedReader(new InputStreamReader(executeProcess.getInputStream()));
            String line;
            while ((line = stdIn.readLine()) != null) {
                logger.info(line);
                String elements[] = line.split("\\s+");
                if(Integer.parseInt(elements[2]) >= 7)
                    return true;

            }
        } catch (Exception e) {
            logger.error("Failed to determine lsusb version", e);
        }
        return false;
    }

    private boolean isFtdiEepromAvailable(){
        try {
            final String[] cmd = new String[]{"ftdi_eeprom"};

            final ProcessBuilder pb = new ProcessBuilder(cmd);
            final Process executeProcess = pb.start();
            //FTDI eeprom generator v0.17

            BufferedReader stdIn = new BufferedReader(new InputStreamReader(executeProcess.getInputStream()));
            String line;
            while ((line = stdIn.readLine()) != null) {
                logger.info(line);
                if(line.contains("v0.17"))
                    return true;

            }
        } catch (Exception e) {
            logger.error("Failed to determine installed ftdi_eeprom", e);
        }
        return false;
    }

    private boolean isOpenocdAvailable(){
        try {
            final String[] cmd = new String[]{"openocd", "-v"};

            final ProcessBuilder pb = new ProcessBuilder(cmd);
            final Process executeProcess = pb.start();

            BufferedReader stdErr = new BufferedReader(new InputStreamReader(executeProcess.getErrorStream()));
            String line;
            while ((line = stdErr.readLine()) != null) {
                logger.info(line);
                if(line.contains("0.10.0"))
                    return true;

            }
        } catch (Exception e) {
            logger.error("Failed to determine installed openocd", e);
        }
        return false;
    }

    private boolean isConfigAvailable() {
        try {
            File ftdiConfig = new File(FTDI.CONFIG_FILE);
            File ocdConfig = new File(Openocd.OPENOCD_CONFIG);
            File firmware = new File(FPA.FIRMWARE_FILE);

            return ftdiConfig.canRead() && ocdConfig.canRead() && firmware.canRead();

        } catch (Exception e) {
            logger.error("Failed to check for config files", e);
        }
        return false;
    }

    public boolean isPreparationPossible() {
        return device != null && device.type.equals(Device.Type.FTDI_UAB);
    }

    public boolean isFirmwareUpdatePossible() {
        return device != null && device.type.equals(Device.Type.FTDI_FPA);
    }

    public boolean isCalibrationPossible() {
        return isFirmwareUpdatePossible() && isComConnectionPossible;
    }

    private void tryConnectCom() {
        String port = findCmdInterface();
        if(port != null) {
            try (AsciiConnection con = new AsciiConnection(port)) {
                String cmd = "get_serial_number\r";
                String response = con.sendMessage(cmd);
                logger.info("sw_version: {}", response);
                String sub = response.substring(2, 5);
                if (response.contains(cmd) && response.contains("450"))
                    isComConnectionPossible = true;
            }
        }else{logger.error("No cmd line interface found");};
    }
}
