package utils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static utils.FPA.logger;

public class FileHelper {
    private final static String UPDATE_SERVER_URL = "http://10.156.4.100/fpa";


    public static void downloadTo(String url, File out)
    {
        URI u = URI.create(url);
        try (InputStream inputStream = u.toURL().openStream()) {
            copyInputStreamToFile(inputStream, out);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void copyInputStreamToFile(InputStream inputStream, File file) {
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            int read;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /***
     * Checks for latest fpa firmware on update server (10.156.4.100/fpa).
     * If current firmware file is older than the one at the server,
     * the newer one will be downloaded and replaces the old one.
     */
    public static void checkForFirmwareUpdate(){
        try {
            //read current firmware
            Path path = Path.of(FPA.FIRMWARE_FILE);
            FileTime creationTimeOfCurrent = (FileTime) Files.getAttribute(path, "creationTime");
            Date dateOfExistent = new Date(creationTimeOfCurrent.toMillis());

            //read on update server
            String firmwareRegex = "(fpa_autosampler.\\d+.\\d+.\\d+.bin).*?(\\d{4}[-]\\d{2}[-]\\d{2}\\s\\d{2}:\\d{2})";
            Map<Integer, String> firmware = FileHelper.searchUpdateServer(firmwareRegex);
            String filename = firmware.get(1);
            //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm", Locale.GERMANY);
            Date serverDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(firmware.get(2));

            if(dateOfExistent.before(serverDate)){
                logger.info("Found newer FPA firmware on update server");
                downloadTo(UPDATE_SERVER_URL, new File(FPA.FIRMWARE_FILE));
            }else{
                logger.info("I am using the latest firmware!");
            }

        } catch (Exception ex) {
            logger.error("Failed to check for firmware updates!");
        }

    }

    private static Map<Integer, String> searchUpdateServer(String regex){
        logger.debug("URL: " + UPDATE_SERVER_URL);

        Map<Integer, String> result = new HashMap<>();
        try {
            URL url = new URL(UPDATE_SERVER_URL);
            URLConnection conn = url.openConnection();
            InputStream inputStream = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            Pattern r = Pattern.compile(regex);

            String line = null;
            while ((line = reader.readLine()) != null) {
                logger.debug(line);
                Matcher m = r.matcher(line);
                if (m.find()) {
                    logger.debug("Found: {} group(s) --> {}", m.groupCount(), m.group());
                    for(int g=1; g<=m.groupCount(); g++){
                        result.put(g, m.group(g));
                    }
                }
            }
            inputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
