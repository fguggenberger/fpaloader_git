package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class SimplePropertyWriter {
    private static final Logger logger = LoggerFactory.getLogger(SimplePropertyWriter.class);

    private String propertiesFile;
    private final Properties properties;

    public SimplePropertyWriter(final String propertiesFile) {
        this.propertiesFile = propertiesFile;
        properties = new Properties();
        loadProperties();
    }

    public String setProperty(final String key, final String value) {

        loadProperties();

        try (FileWriter out = new FileWriter(propertiesFile)) {

            logger.info("Properties key = {}", key);
            logger.info("Properties value = {}", value);
            properties.setProperty(key, value);
            properties.store(out, null);

        } catch (IOException e) {
            logger.warn("Could not find {}", propertiesFile);
        }
        return value;
    }

    private void loadProperties() {
        try (FileReader in = new FileReader(propertiesFile)) {
            properties.load(in);

        } catch (IOException e) {
            logger.warn("Could not find {}", propertiesFile);
        }
    }
}
