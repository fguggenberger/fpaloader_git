package utils;

public class Device {
    public enum Type{
        NOT_INITIALIZED(null, null),
        UNKNOWN(null, null),
        FTDI_UAB("Quad RS232-HS", ""),
        FTDI_FPA( "USB Serial Converter","Ftdi4Fpa");

        //COM();

        String iManufacturer = "FTDI";  //1 FTDI
        String iProduct;                //2 Quad RS232-HS
        String iSerial;                 //0
        String port;
        // iManufacturer                //1 FTDI
        // iProduct                     //2 USB Serial Converter
        // iSerial                      //3 fpa52WA7A



        Type(String product, String iSerial){
            this.iProduct = product;
            this.iSerial = iSerial;
        }
        Type(String port){
            this.port = port;
        }
    }
    private String port;
    private String iSerial;
    private String description;
    private String manufacturer;
    private String product;

    Type type;
    public Device() {
        this.type = Type.NOT_INITIALIZED;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getiSerial() {
        return iSerial;
    }

    public void setSerial(String iSerial) {
        if(iSerial != null && iSerial.contains(Type.FTDI_FPA.iSerial))
            this.type = Type.FTDI_FPA;
        else if(iSerial != null && iSerial.isEmpty())
            this.type = Type.FTDI_UAB;
        else
            this.type = Type.UNKNOWN;
        this.iSerial = iSerial;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    @Override
    public String toString(){
        return "Device: " + type.toString() + "(" + iSerial +")";
    }
}
