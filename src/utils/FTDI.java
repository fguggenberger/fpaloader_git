package utils;

import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Field;

public class FTDI {
    final static Logger logger = LoggerFactory.getLogger(FTDI.class);

    final static String CONFIG_FILE = "/home/grabner/ftdi.properties";

    public static Task<Boolean> getSetNewSerialTask(){
        return new Task<Boolean>(){
            @Override
            protected Boolean call() throws Exception {
                boolean succeeded = false;
                try {
                    String msg = "Setting new serial to FTDI...";
                    updateMessage(msg);
                    String newSerial = "Ftdi4Fpa";
                    logger.info("Trying to set serial({}) for ftdi ...", newSerial);

                    //flash ftdi eeprom
                    final String[] cmd = new String[]{"ftdi_eeprom", "--flash-eeprom", CONFIG_FILE};

                    final ProcessBuilder pb = new ProcessBuilder(cmd);
                    final Process executeProcess = pb.start();

                    BufferedReader stdIn = new BufferedReader(new InputStreamReader(executeProcess.getInputStream()));
                    String line;
                    while ((line = stdIn.readLine()) != null) {
                        logger.info(line);
                        updateMessage(line);
                        if(line.contains("FTDI close:"))
                            succeeded = true;

                    }
                } catch (Exception e) {
                    logger.error("Failed to write new serial to eeprom", e);
                }
                return succeeded;
            }
        };
    }

    public static void erase() {
        boolean succeeded = false;
        try {
            //erase ftdi eeprom
            final String[] cmd = new String[]{"ftdi_eeprom", "--erase-eeprom", CONFIG_FILE};

            final ProcessBuilder pb = new ProcessBuilder(cmd);
            final Process executeProcess = pb.start();

            BufferedReader stdIn = new BufferedReader(new InputStreamReader(executeProcess.getInputStream()));
            String line;
            while ((line = stdIn.readLine()) != null) {
                logger.info(line);
            }
        } catch (Exception e) {
            logger.error("Failed to erase eeprom", e);
        }
    }

    public static Device find(){
        //Todo: return list of devices instead of one single
        logger.info("Trying to find ftdi devices...");
        Device d = new Device();

        final String[] cmd = new String[]{"lsusb", "-v", "-d 0403:6011"};

        try {
            final ProcessBuilder pb = new ProcessBuilder(cmd).redirectError(ProcessBuilder.Redirect.to(new File("lsusb.err")));
            final Process executeProcess = pb.start();

            BufferedReader stdIn = new BufferedReader(new InputStreamReader(executeProcess.getInputStream()));
            String line;
            while ((line = stdIn.readLine()) != null) {
                logger.info(line);
                if(line.contains("iManufacturer"))
                    d.setManufacturer(filterLsusb(line));
                if(line.contains("iProduct"))
                    d.setProduct(filterLsusb(line));
                if(line.contains("iSerial"))
                    d.setSerial(filterLsusb(line));
            }
        }catch (Exception e){
            logger.error("Failed to lsusb -d", e);
        }

        return d;
    }

    private static String filterLsusb(String line) {
        try {
            return line.substring(28);
        } catch (Exception e) {
            logger.error("Failed to filter lsusb output", e);
        }
        return null;
    }


}
