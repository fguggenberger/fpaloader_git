package openocd;

/*
OpenOCD System variables:
_CHIPNAME       // Name of the chip (e.g. sam4)
_TARGETNAME     // $_CHIPNAME.<tapType>
 */

import java.io.Closeable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 * Wrapper class for the OpenOCDWrapper RPC-API
 *
 * @author Johannes Wiesböck
 * @version 0.1
 */
@SuppressWarnings("WeakerAccess")
public class OpenOCDWrapper implements Closeable {

    private InterfaceWrapper INTERFACE = null;

    public final SystemFunction SYS = new SystemFunction();
    public final ServerCommands SERVER = new ServerCommands();
    public final AdapterCommands ADAPTER = new AdapterCommands();
    public final ResetConfig RESET_CONFIG = new ResetConfig();
    public final TapCommands TAP = new TapCommands();
    public final TargetCommands TARGET = new TargetCommands();
    public final FlashCommands FLASH = new FlashCommands();
    public final GeneralCommands GENERAL = new GeneralCommands();
    public final ArchitectureAndCoreCommands ARCH_CORE = new ArchitectureAndCoreCommands();

    public class SystemFunction {
        /**
         * Checks whether a given String is a valid integer number. The radix can be either hexadecimal or decimal.
         *
         * @param s String to be checked
         * @return true if s is a valid number, false otherwise
         */
        protected boolean isNumber(String s) {
            try {
                getLong(s);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }

        /**
         * Decodes a string (hex or decimal) to a long value
         *
         * @param s String to be decoded
         * @return Decoded value
         * @throws NumberFormatException Thrown if the string is illegally formatted or in case of an overflow
         */
        protected long getLong(String s) {
            return Long.decode(s.replaceAll("[^A-Fa-f0-9xX#]", ""));
        }

        /**
         * Decodes a string (hex or decimal) to an integer value
         *
         * @param s String to be decoded
         * @return Decoded value
         * @throws NumberFormatException Thrown if the string is illegally formatted or in case of an overflow
         */
        protected int getInt(String s) {
            return Integer.decode(s.replaceAll("[^A-Fa-f0-9xX#]", ""));
        }

        /**
         * Decodes a string (hex or decimal) to a short value
         *
         * @param s String to be decoded
         * @return Decoded value
         * @throws NumberFormatException Thrown if the string is illegally formatted or in case of an overflow
         */
        protected short getShort(String s) {
            return Short.decode(s.replaceAll("[^A-Fa-f0-9xX#]", ""));
        }

        /**
         * Decodes a string (hex or decimal) to a byte value
         *
         * @param s String to be decoded
         * @return Decoded value
         * @throws NumberFormatException Thrown if the string is illegally formatted or in case of an overflow
         */
        protected byte getByte(String s) {
            return Byte.decode(s.replaceAll("[^A-Fa-f0-9xX#]", ""));
        }
    }

    public class ServerCommands {
        private boolean initialized = false;

        public void enableTclTrace() throws IOException {
            INTERFACE.transmit("tcl_trace on");
        }

        public void disableTclTrace() throws IOException {
            INTERFACE.transmit("tcl_trace off");
        }

        /**
         * Enables tcl notifications. When called, target event, state and reset notifications are emitted as
         * Tcl associative arrays.
         *
         * @throws IOException
         */
        public void enableTclNotifications() throws IOException {
            INTERFACE.transmit("tcl_notifications on");
        }

        /**
         * Disables tcl notifications.
         *
         * @throws IOException
         */
        public void disableTclNotifications() throws IOException {
            INTERFACE.transmit("tcl_notifications off");
        }

        public boolean isInitialized() {
            return initialized;
        }

        public void setGdbPort(int port) throws IOException {
            INTERFACE.transmit("ocd_gdb_port " + port);
        }

        public int getGdbPort() throws IOException {
            String s = INTERFACE.transmit("ocd_gdb_port");
            if (s.contains("disable")) {
                return -1;
            }
            return SYS.getInt(s.replaceAll("[^0-9]", ""));
        }

        public void disableGdb() throws IOException {
            INTERFACE.transmit("ocd_gdb_port disable");
        }

        public void setTelnetPort(int port) throws IOException {
            INTERFACE.transmit("ocd_telnet_port " + port);
        }

        public int getTelnetPort() throws IOException {
            String s = INTERFACE.transmit("ocd_telnet_port");
            if (s.contains("disable")) {
                return -1;
            }
            return SYS.getInt(s.replaceAll("[^0-9]", ""));
        }

        public void disableTelnet() throws IOException {
            INTERFACE.transmit("ocd_telnet_port disabled");
        }

        public void setTclPort(int port) throws IOException {
            INTERFACE.transmit("ocd_tcl_port " + port);
        }

        public int getTclPort() throws IOException {
            String s = INTERFACE.transmit("ocd_tcl_port");
            if (s.contains("disable")) {
                return -1;
            }
            return SYS.getInt(s.replaceAll("[^0-9]", ""));
        }

        public void disableTcl() throws IOException {
            INTERFACE.transmit("ocd_tcl_port disable");
        }

        public void init() throws IOException {
            INTERFACE.transmit("ocd_init");
            initialized = true;
        }

        public void disableGdbBreakpoints() throws IOException {
            INTERFACE.transmit("ocd_gdb_breakpoint_override disable");
        }

        public void setGdbBreakpointType(boolean hard) throws IOException {
            INTERFACE.transmit("ocd_gdb_breakpoint_oeverride " + (hard ? "hard" : "soft"));
        }

        public void setGdbFlashProgramming(boolean enable) throws IOException {
            INTERFACE.transmit("ocd_gdb_flash_program " + (enable ? "enable" : "disable"));
        }

        public void setGdbMemoryMap(boolean enable) throws IOException {
            INTERFACE.transmit("ocd_gdb_memory_map " + (enable ? "enable" : "disable"));
        }

        public void setGdbReportDataAbort(boolean enable) throws IOException {
            INTERFACE.transmit("ocd_gdb_report_data_abort " + (enable ? "enable" : "disable"));
        }

        public void setGdbSendTargetDescription(boolean enable) throws IOException {
            INTERFACE.transmit("ocd_gdb_target_description " + (enable ? "enable" : "disable"));
        }

        public void gdbSaveTargetDescription() throws IOException {
            INTERFACE.transmit("ocd_gdb_save_tdesc");
        }

        public void enablePolling() throws IOException {
            INTERFACE.transmit("ocd_poll on");
        }

        public void disablePolling() throws IOException {
            INTERFACE.transmit("ocd_poll off");
        }

        public boolean getPolling() throws IOException {
            return INTERFACE.transmit("ocd_poll").contains("on");
        }
    }

    public class AdapterCommands {
        public Collection<String> getSupportedTransportTypes() throws IOException {
            String s = INTERFACE.transmit("ocd_transport list");
            return Arrays.stream(s.split(System.lineSeparator())).filter(e -> !e.contains(":"))
                    .map(e -> e.replaceAll("[^a-zA-z-_]", ""))
                    .collect(Collectors.toList());
        }

        public String getTransportType() throws IOException {
            String s = INTERFACE.transmit("ocd_transport select");
            if (s.contains("does not support")) {
                return null;
            }
            return s;
        }

        public void setTransportType(String transportType) throws IOException {
            INTERFACE.transmit("ocd_transport select " + transportType);
        }

        public void setClockSpeed(long frequency) throws IOException {
            if (frequency < 0) {
                throw new OpenOCDException("OpenOCDWrapper.ADAPTER.setClockSpeed: Cannot set a clock speed < 0");
            }
            if (frequency == 0) {
                throw new OpenOCDException("OpenOCDWrapper.ADAPTER.setClockSpeed: RTCK method is not supported");
            }
            INTERFACE.transmit("adapter_khz " + frequency / 1000);
        }

        public long getClockSpeed() throws IOException {
            String s = INTERFACE.transmit("adapter_khz").replaceAll("[^0-9]", "");
            if (SYS.isNumber(s)) {
                return SYS.getLong(s) * 1000;
            }
            throw new OpenOCDException("OpenOCDWrapper..ADAPTERgetClockSpeed: Cannot parse response", s);
        }

        public void setRclkSpeed(long frequency) throws IOException {
            INTERFACE.transmit("jtag_rclk " + frequency / 1000);
        }

        public long getRclkSpeed() throws IOException {
            String s = INTERFACE.transmit("jtag_rclk").replaceAll("[^0-9]", "");
            if (SYS.isNumber(s)) {
                return SYS.getLong(s) * 1000;
            }
            throw new OpenOCDException("OpenOCDWrapper.ADAPTER.setRclkSpeed: Could not parse value");
        }

        public String getAdapterName() throws IOException {
            return INTERFACE.transmit("adapter_name");
        }

        public void setInterface(String interfaceName) throws IOException {
            INTERFACE.transmit("ocd_interface " + interfaceName);
        }

        public Collection<String> getInterfaceList() throws IOException {
            return Arrays.stream(INTERFACE.transmit("ocd_interface_list").split(System.lineSeparator()))
                    .filter(e -> Character.isDigit(e.charAt(0)))
                    .map(e -> e.split(" ")[1])
                    .collect(Collectors.toList());
        }

        public void ftdi_vid_pid(long vid, long pid) throws IOException {
            INTERFACE.transmit("ocd_ftdi_vid_pid " + vid + " " + pid);
        }

        public void ftdi_device_desc(String description) throws IOException {
            INTERFACE.transmit("ocd_ftdi_device_desc " + description);
        }

        public void ftdi_serial(String serialNumber) throws IOException {
            INTERFACE.transmit("ocd_ftdi_serial " + serialNumber);
        }

        public void ftdi_location(LinkedList<Integer> location) throws IOException {

            LinkedList<Integer> location_copy = new LinkedList<>(location);
            String loc = location_copy.removeFirst().toString();
            while (!location_copy.isEmpty()) {
                loc += "," + location_copy.removeFirst().toString();
            }
            INTERFACE.transmit("ocd_ftdi_location " + loc);
        }

        public void ftdi_channel(int channel) throws IOException {
            INTERFACE.transmit("ocd_ftdi_channel " + channel);
        }

        public void ftdi_layout_init(Collection<FtdiGpioSignal> signals) throws IOException {
            long data = FtdiGpioSignal.generateDefaultDataMask(signals);
            long direction = FtdiGpioSignal.generateDefaultDirectionMask(signals);
            INTERFACE.transmit("ocd_ftdi_layout_init " + data + " " + direction);
        }

        public void ftdi_layout_signal(FtdiGpioSignal signal) throws IOException {
            String config = signal.getConfig();
            INTERFACE.transmit("ocd_ftdi_layout_signal " + config);
        }

        /**
         * Sets a GPIO signal of the FTDI chip. The signal first has to
         * be specified in the servers configuration file.
         *
         * @param signal Signal to be set
         * @param value  Can be 0 | 1 | z (according to the pin configuration)
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown if the pin cannot be set to the given value
         */
        public void ftdi_set_signal(FtdiGpioSignal signal, FtdiGpioSignal.PinState value) throws IOException {
            String s = INTERFACE.transmit("ocd_ftdi_set_signal " + signal.getName() + " " + value.toString());
            if (s.contains("cannot")) {
                throw new OpenOCDException("OpenOCDWrapper.ADAPTER.ftdi_set_signal: Cannot set the pin to the specified value", s);
            }
        }

        /**
         * Captures the pin state of a predefined GPIO pin
         *
         * @param signal GPIO pin to be captured
         * @return 1 if the pin was high, 0 otherwise
         * @throws IOException Thrown in case of a communication error
         */
        public FtdiGpioSignal.PinState ftdi_get_signal(FtdiGpioSignal signal) throws IOException {
            String s = INTERFACE.transmit("ocd_ftdi_get_signal " + signal.getName());
            if (s.substring(10 + signal.getName().length(), s.length() - 1).contains("1")) {
                return FtdiGpioSignal.PinState.HIGH;
            }
            return FtdiGpioSignal.PinState.LOW;
        }

        /**
         * Configure TCK edge at which the adapter samples the value of the TDO signal.
         * Due to signal propagation delays, sampling TDO on rising TCK can become quite peculiar at high JTAG clock
         * speeds. However, FTDI chips offer a possibility to sample TDO on falling edge of TCK which may
         * increase stability at higher JTAG clocks.
         *
         * @param rising If true, the TDO signal will be sampled at the rising edge
         * @throws IOException Thrown in case of a communication error
         */
        public void ftdi_tdo_sample_edge(boolean rising) throws IOException {
            INTERFACE.transmit("ftdi_tdo_sample_edge " + (rising ? "rising" : "falling"));
        }
    }

    public class ResetConfig {
        private final long NSRST_MIN_ASSERT_WIDTH_MS = 50;
        private final long NSRST_MIN_DELAY_MS = 300;
        private final long NTRST_MIN_ASSERT_WIDTH_MS = 50;
        private final long NTRST_MIN_DELAY_MS = 300;

        /**
         * Sets the minimum amount of time OpenOCDWrapper should wait after asserting nSRST (active-low system reset)
         * before allowing it to be deasserted
         */
        public void setSRSTAssertWidth(long milliseconds) throws IOException {

            if (milliseconds < NSRST_MIN_ASSERT_WIDTH_MS) {
                throw new OpenOCDException("OpenOCDWrapper.RESET.setSRSTAssertWidth: Cannot assert nSRST for less than " + NSRST_MIN_ASSERT_WIDTH_MS + "ms");
            }

            INTERFACE.transmit("adapter_nsrst_assert_width " + milliseconds);
        }

        /**
         * Sets how long OpenOCDWrapper should wait after deasserting nSRST (active low system reset) before
         * starting new JTAG operations. When a board has a reset button connected to SRST line it will probably have hardware
         * debouncing, implying you should use this
         */
        public void setSRSTDelay(long milliseconds) throws IOException {
            if (milliseconds < NSRST_MIN_DELAY_MS) {
                throw new OpenOCDException("OpenOCDWrapper.RESET.setSRSTDelay: Cannot use a delay less than " + NSRST_MIN_DELAY_MS + "ms");
            }
            INTERFACE.transmit("adapter_nsrst_delay " + milliseconds);
        }

        /**
         * Sets the minimum amount of time OpenOCDWrapper should wait after asserting nTRST (active-low JTAG TAP reset)
         * before allowing it to be deasserted
         */
        public void setTRSTAssertWidth(long milliseconds) throws IOException {

            if (milliseconds < NTRST_MIN_ASSERT_WIDTH_MS) {
                throw new OpenOCDException("OpenOCDWrapper.RESET.setSRSTAssertWidth: Cannot assert nSRST for less than " + NSRST_MIN_ASSERT_WIDTH_MS + "ms");
            }
            INTERFACE.transmit("jtag_ntrst_assert_width " + milliseconds);
        }

        /**
         * Sets how long OpenOCDWrapper should wait after deasserting nSRST (active-low JTAG TAP reset) before
         * starting new JTAG operations. When a board has a reset button connected to SRST line it will probably have hardware
         * debouncing, implying you should use this
         */
        public void setTRSTDelay(long milliseconds) throws IOException {
            if (milliseconds < NTRST_MIN_DELAY_MS) {
                throw new OpenOCDException("OpenOCDWrapper.RESET.setSRSTDelay: Cannot use a delay less than " + NSRST_MIN_DELAY_MS + "ms");
            }
            INTERFACE.transmit("jtag_ntrst_delay " + milliseconds);
        }

        public void setResetConfig(ResetSignal.ResetSignalType type,
                                   ResetSignal.ResetSignalCombination combination,
                                   ResetSignal.ResetSignalGating gating,
                                   ResetSignal.ResetSignalTrstType trstType,
                                   ResetSignal.ResetSignalSrstType srstType,
                                   ResetSignal.ResetSignalConnectType connectType) throws IOException {
            String msg = "reset_config ";
            msg += type.toString().toLowerCase();
            if (gating != null) {
                msg += " " + gating.toString().toLowerCase();
            }
            if (connectType != null) {
                msg += " " + connectType.toString().toLowerCase();
            }

            if (combination != null) {
                msg += " " + combination.toString().toLowerCase();
            }
            if (srstType != null) {
                msg += " " + srstType.toString().toLowerCase();
            }
            if (trstType != null) {
                msg += " " + trstType.toString().toLowerCase();
            }
            INTERFACE.transmit(msg);
        }

        //TODO: Custom reset handling via handlers
    }

    public class TapCommands {
        /**
         * Creates a new JTAG tap. Must be called before adding a cpu target.
         *
         * @param name              Name of the chip. Could be "STM32F4" or "SAM4". Should follow "C" symbol name rules.
         * @param tapType           Declares the type of the tap. (E.g. cpu, etb, ...)
         * @param irLen             The length in bits of the instruction register, such as 4 or 5
         * @param irCapture         The bit pattern loaded by the tap into the JTAG shift register on entry
         *                          of the IRCAPTURE state, such as 0x01. JTAG requires the two LSBs of this value to be 01.
         * @param irMask            A mask used with -ircapture to verify that instruction scans work correctly. Such
         *                          scans are not used by OpenOCD excwept to verify that there seems to be no
         *                          problems with JTAG scan chain operations.
         * @param expectedId        A non-zero number representing an IDCODE which you expect to find when the scan chain
         *                          is examined. These codes are not required by all JTAG devices. Set to 0 if the id should
         *                          be ignored.
         * @param ignoreJtagVersion If true, the JTAG version field in the excpedtedId-Field is ignored
         * @throws IOException Thrown in case of a communication error
         */
        public void createTAP(String name,
                              JtagTapType tapType,
                              int irLen, int irCapture,
                              int irMask, long expectedId,
                              boolean ignoreJtagVersion) throws IOException {
            INTERFACE.transmit("set _CHIPNAME " + name);
            INTERFACE.transmit("set _TARGETNAME " + name + "." + tapType.toString().toLowerCase());
            String s = "jtag newtap " + name + " "
                    + tapType.toString().toLowerCase()
                    + " -irlen " + irLen
                    + " -ircapture " + irCapture
                    + " -irmask " + irMask
                    + " -expected-id " + expectedId
                    + (ignoreJtagVersion ? " -ignore-version" : "");
            System.out.println(s);
            INTERFACE.transmit(s);
        }

        /**
         * Creates a new JTAG tap. Must be called before adding a cpu target.
         *
         * @param name    Name of the chip. Could be "STM32F4" or "SAM4". Should follow "C" symbol name rules.
         * @param tapType Declares the type of the tap.
         * @param irLen   The length in bits of the instruction register, such as 4 or 5
         * @throws IOException Thrown in case of a communication error
         */
        public void createTAP(String name, JtagTapType tapType, int irLen) throws IOException {
            INTERFACE.transmit("set _CHIPNAME " + name);
            INTERFACE.transmit("set _TARGETNAME " + name + "." + tapType.toString().toLowerCase());
            INTERFACE.transmit("jtag newtap " + name + " "
                    + tapType.toString().toLowerCase()
                    + " -irlen " + irLen);
        }
    }

    public class TargetCommands {
        public long at91sam4_getSlowClockFrequency() throws IOException {
            return Long.parseLong(INTERFACE.transmit("ocd_at91sam4 slowclk").replaceAll("[^0-9]", ""));
        }

        public void at91sam4_setSlowClockFrequency(long frequency) throws IOException {
            INTERFACE.transmit("ocd_at91sam4 slowclk " + frequency);
        }

        public String getInfo() throws IOException {
            return INTERFACE.transmit("ocd_at91sam4 info");
        }

        public void at91sam4_setSecurityBit() throws IOException {
            INTERFACE.transmit("at91sam4 gpnvm set 0");
        }

        public void at91sam4_clearSecurityBit() throws IOException {
            INTERFACE.transmit("at91sam4 gpnvm clear 0");
        }

        public void at91sam4_setBootModeFlash() throws IOException {
            INTERFACE.transmit("at91sam4 gpnvm set 1");
        }

        public void at91sam4_setBootModeROM() throws IOException {
            INTERFACE.transmit("at91sam4 gpnvm clear 1");
        }

        public String getTargetName() throws IOException {
            return INTERFACE.transmit("ocd_echo $_TARGETNAME");
        }

        public void addTarget(String targetType, boolean isLittleEndian) throws IOException {
            INTERFACE.transmit("target create $_TARGETNAME "
                    + targetType.toLowerCase()
                    + (isLittleEndian ? " -endian little " : " -endian big ")
                    + "-chain-position $_TARGETNAME");
        }

        /**
         * Specify the work area size in bytes
         *
         * @throws IOException
         */
        public void setWorkAreaSize(long size) throws IOException {
            INTERFACE.transmit("$_TARGETNAME configure -work-area-size " + size);
        }

        /**
         * Enable backing up of the work area. If possible, this should not be used since it slows
         * down operations.
         *
         * @throws IOException
         */
        public void enableWorkAreaBackup() throws IOException {
            INTERFACE.transmit("$_TARGETNAME configure -work-area-backup 1");
        }

        /**
         * Disable backing up of the work area. Not necessary to be called, since this is the default
         *
         * @throws IOException
         */
        public void disableWorkAreaBackup() throws IOException {
            INTERFACE.transmit("$_TARGETNAME configure -work-area-backup 0");
        }

        /**
         * Sets the work area base address to be used when no MMU is active
         *
         * @param address Physical base address
         * @throws IOException
         */
        public void setWorkAreaAddressPhys(long address) throws IOException {
            INTERFACE.transmit("$_TARGETNAME configure -work-area-phys " + address);
        }

        /**
         * Sets the work area base address to be used when an MMU is active. Do not specify a value for this
         * except on targets with an MMU.
         *
         * @param address Virtual base address
         * @throws IOException
         */
        public void setWorkAreaAddressVirt(long address) throws IOException {
            INTERFACE.transmit("$_TARGETNAME configure -work-area-virt " + address);
        }

        /**
         * Enable rtos support for target
         *
         * @param type Type of the used rtos. Can be auto, eCos, ThreadX, FreeRTOS, linux, ChibiOS, embKernel, mqx or uCOS-III
         * @throws IOException
         */
        public void setRtosType(String type) throws IOException {
            INTERFACE.transmit("$_TARGETNAME configure -rtos " + type);
        }

        /**
         * Returns the names of all available targets
         *
         * @return Names of all available targets
         * @throws IOException Thrown in case of a communication error
         */
        public String[] getTargetNames() throws IOException {
            String s = INTERFACE.transmit("ocd_target names");
            return s.split(System.lineSeparator());
        }

        /**
         * Returns all supported target CPU types
         *
         * @return Supported CPU types
         * @throws IOException Thrown in case of a communication error
         */
        public String[] getSupportedTargetTypes() throws IOException {
            String s = INTERFACE.transmit("ocd_target types");
            return s.split(" ");
        }

        /**
         * Sets the currently active target to the given target. This is only relevant on boards which
         * have more than one target.
         *
         * @param targetName Target to be set active
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an unknown target
         */
        public void selectTarget(String targetName) throws IOException {
            String s = INTERFACE.transmit("ocd_targets " + targetName);
            if (!s.isEmpty()) {
                throw new OpenOCDException("OpenOCDWrapper.TARGETS.selectTarget: Could not switch to specified target", s);
            }
        }

        /**
         * Returns the current state of the active target
         *
         * @return State of the target
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of a parsing error
         */
        public TargetState getState() throws IOException {
            String s = INTERFACE.transmit("$_TARGETNAME curstate");
            for (TargetState state : TargetState.values()) {
                if (s.contains(state.toString())) {
                    return state;
                }
            }
            throw new OpenOCDException("OpenOCDWrapper.TARGET.getState: Could not get target state", s);
        }
    }

    public class FlashCommands {

        public long getBaseAddress(long bank) throws IOException {
            return SYS.getLong(Arrays.stream(INTERFACE.transmit("ocd_flash banks").split(System.lineSeparator()))
                    .filter(line -> Long.parseLong(String.valueOf(line.charAt(1))) == bank)
                    .findAny().get().split(" ")[6]);
        }

        public long getBaseAddress() throws IOException {
            return SYS.getLong(INTERFACE.transmit("ocd_flash banks").split(" ")[5]);
        }

        public void addBank(String flashDriverName, long baseAddress, long bankNumber, long busWidth, long chipWidth) throws IOException {
            INTERFACE.transmit("flash bank $_CHIPNAME.flash "
                    + flashDriverName + " "
                    + baseAddress + " "
                    + bankNumber + " "
                    + busWidth + " "
                    + chipWidth
                    + " $_TARGETNAME");
        }

        public String[] getInfo() throws IOException {
            return INTERFACE.transmit("flash banks").split(System.lineSeparator());
        }

        public void eraseSectors(int bank, int firstSector, int lastSector) throws IOException {
            String s = INTERFACE.transmit("flash probe " + bank);
            if (!s.contains("found at")) {
                throw new OpenOCDException("OpenOCDWrapper.FLASH.eraseSectors: Could not find specified bank", s);
            }
            if (firstSector < 0 || firstSector > lastSector) {
                throw new OpenOCDException("OpenOCDWrapper.FLASH.eraseSectors: Invalid sector range (", +firstSector + "," + lastSector + ")");
            }
            s = INTERFACE.transmit("ocd_flash erase_sector " + bank + " " + firstSector + " " + lastSector);
            if (!s.contains("erased sectors " + firstSector + " through " + lastSector + " on flash bank " + bank)) {
                throw new OpenOCDException("OpenOCDWrapper.FLASH.eraseSectors: Error", s);
            }
        }

        public void clearProtection(long bank) throws IOException {
            String s = INTERFACE.transmit("ocd_flash protect " + bank + " 0 last off");
            if (!s.contains("cleared protection")) {
                throw new OpenOCDException("OpenOCDWrapper.FLASH.clearProtection: " + s);
            }
        }

        public void setProtection(long bank) throws IOException {
            String s = INTERFACE.transmit("ocd_flash protect " + bank + " 0 last on");
            if (!s.contains("set protection for sectors")) {
                throw new OpenOCDException("OpenOCDWrapper.FLASH.setProtection: " + s);
            }
        }

        public void program(String filename) throws IOException {
            String s = INTERFACE.transmit("program " + filename + " " + FLASH.getBaseAddress());
            if (s.contains("Error")) {
                throw new OpenOCDException("OpenOCDWrapper.FLASH.program: Could not program image", s);
            }

        }
    }

    public class GeneralCommands {
        @Deprecated
        public void exit() throws IOException {
            INTERFACE.transmit("ocd_exit");
        }

        public String help(String msg) throws IOException {
            return INTERFACE.transmit("ocd_help " + msg);
        }

        /**
         * Sets the server in sleep mode for the specified duration. The method returns when the specified
         * time has passed or an exception is thrown.
         *
         * @param milliseconds Time to be waited in milliseconds
         * @param busy         If true, the server issues a busy wait, otherwise interrupt-based
         * @throws IOException      Thrown in case of a communcation error
         * @throws OpenOCDException Thrown in case of a wait-time < 0 ms
         */
        public void sleep(long milliseconds, boolean busy) throws IOException {
            if (milliseconds < 0) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.sleep: Cannot wait for less than 0 ms");
            }
            INTERFACE.transmit("ocd_sleep " + milliseconds + (busy ? " busy" : ""));
        }

        public void shutdownServer() throws IOException {
            String s = INTERFACE.transmit("ocd_shutdown");
            if (!s.contains("shutdown command invoked")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.shutdown: Could not issue command", s);
            }
        }

        public void shutdownServer(int errorCode) throws IOException {
            String s = INTERFACE.transmit("ocd_shutdown " + errorCode);
            if (!s.equals("shutdown command invoked")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.shutdown: Could not issue command", s);
            }
        }

        /**
         * Returns the currently set debug level where
         * 0 ... Error messages only
         * 1 ... add warnings
         * 2 ... add informational messages
         * 3 ... add debugging messages
         *
         * @return The currently set debug level
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown if the return value cannot be parsed
         */
        public long getDebugLevel() throws IOException {
            String s = INTERFACE.transmit("ocd_debug_level");
            String num = s.replaceAll("[^0-3]", "");
            if (SYS.isNumber(num)) {
                return SYS.getLong(num);
            }
            throw new OpenOCDException("OpenOCDWrapper.GENERAL.getDebugLevel: Could not parse response", s);
        }

        /**
         * Sets the current debug level where
         * 0 ... Error messages only
         * 1 ... add warnings
         * 2 ... add informational messages
         * 3 ... add debugging messages
         *
         * @param level Level to be set (allowed range is 0...3)
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of param level being out of bounds
         */
        public void setDebugLevel(int level) throws IOException {
            if (level < 0) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.setDebugLevel: Cannot set a debug level < 0");
            }
            if (level > 3) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.setDebugLevel: Cannot set a debug level > 3");
            }
            INTERFACE.transmit("ocd_debug_level " + level);
        }

        /**
         * Echoes the given parameter at the host's console
         *
         * @param msg                     Message to be echoed
         * @param suppressTrailingNewLine Suppresses the new line character if true
         * @throws IOException Thrown in case of a communication error
         */
        public void echo(String msg, boolean suppressTrailingNewLine) throws IOException {
            if (suppressTrailingNewLine) {
                INTERFACE.transmit("ocd_echo -n \"" + msg + "\"");
            } else {
                INTERFACE.transmit("ocd_echo \"" + msg + "\"");
            }
        }

        /**
         * Redirects logging to a logfile; the initial log output channel is stderr.
         *
         * @param filename Logfile
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an invalid logfile path
         */
        public void setLogFile(String filename) throws IOException {
            String s = INTERFACE.transmit("ocd_log_output " + filename);
            if (!s.isEmpty()) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.log_output: Error", s);
            }
        }

        /**
         * Adds a directory to the file/script search path
         *
         * @param directory Directory to be added
         * @throws IOException Thrown in case of a communication error
         */
        public void addScriptSearchDirectory(String directory) throws IOException {
            INTERFACE.transmit("ocd_add_script_search_dir" + directory);
        }

        /**
         * Specify address by name on which to listen for incoming TCP/IP connections.
         * By default, OpenOCDWrapper will listen on all available interfaces
         *
         * @param interfaceName Interface to be listened on
         * @throws IOException Thrown in case of a communication error
         */
        public void bindToInterface(String interfaceName) throws IOException {
            INTERFACE.transmit("ocd_bindto " + interfaceName);
        }

        /**
         * Retrieves the contents of the register file in a user-friendly form
         * showing number, name, size, value and cache status. For valid entries, a value
         * is shown; valid entries which are also dirty (and will be written back later) are
         * flagged as such. The target must generally be halted before access to core
         * registers is allowed. Depending on the hardware, some other registers may be
         * accessible while the target is running.
         *
         * @return Contents of the register file
         * @throws IOException Thrown in case of a communication error
         */
        public String reg() throws IOException {
            return INTERFACE.transmit("ocd_reg");
        }

        /**
         * Display the value of the specified register. If specified the internal cache
         * can be bypassed. The target must generally be halted before access to core
         * registers is allowed. Depending on the hardware, some other registers may be
         * accessible while the target is running.
         *
         * @param identifier Number/name of the register
         * @param force      If true, the internal cache is bypassed
         * @return Value of the register.
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an unknown register or a parsing error
         */
        public long reg(String identifier, boolean force) throws IOException {
            String s = INTERFACE.transmit("ocd_reg " + identifier + (force ? " force" : ""));
            if (s.isEmpty()) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.reg: Cannot read register " + identifier + ". Target must be halted.");
            }
            if (s.contains("not found")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.reg: " + s, s);
            }
            String val = s.split(" ")[2];
            if (SYS.isNumber(val)) {
                return SYS.getLong(val);
            }
            throw new OpenOCDException("OpenOCDWrapper.GENERAL.reg: Could not parse value of register " + identifier, s);
        }

        /**
         * Sets the register's value. Writes may be held in a writeback cache internal to OpenOCDWrapper, so that
         * setting the value marks the register as dirty instead of immediately flushing that value. Resuming
         * CPU execution (including single stepping) or otherwise activating the relevant module will flush such
         * values.
         *
         * @param identifier Name/number of the register
         * @param value      Value that should be written
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an unknown register or a parsing error
         */
        public void reg(String identifier, long value) throws IOException {
            String s = INTERFACE.transmit("ocd_reg " + identifier + " " + value);
            if (s.isEmpty()) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.reg: Cannot write to register " + identifier);
            }
            if (s.contains("not found")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.reg: " + s, s);
            }
        }

        /**
         * Halts the currently active target. If the target is in reset state,
         * a second halt command has to be issued. The waiting period for the target
         * is up to 5 seconds.
         *
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException In case the target state has not changed to halted
         */
        public void halt() throws IOException {
            // INTERFACE.transmit("ocd_halt");
            String s = INTERFACE.transmit("ocd_halt");
            if (TARGET.getState() != TargetState.HALTED) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.halt: Could not halt target", s);
            }
        }

        /**
         * Halts the currently active target. If the target is in reset state,
         * a second halt command has to be issued. The waiting period for the target
         * can be specified.
         *
         * @param maxWaitMilliseconds Maximal waiting time
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an illegal waiting time or if the target state has not changed to halted
         */
        public void halt(long maxWaitMilliseconds) throws IOException {

            if (maxWaitMilliseconds < 0) {
                throw new OpenOCDException("OpenOCDWrapper.halt: Cannot wait for a negative time");
            }
            if (TARGET.getState() == TargetState.RESET) {
                INTERFACE.transmit("halt " + maxWaitMilliseconds);
            }
            String s = INTERFACE.transmit("ocd_halt " + maxWaitMilliseconds);
            if (TARGET.getState() != TargetState.HALTED) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.halt: Could not halt target", s);
            }
        }

        /**
         * Behaves like halt() except sending a halt request to the target.
         *
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of a timeout
         */
        @Deprecated
        public void wait_halt() throws IOException {
            String s = INTERFACE.transmit("ocd_wait_halt");
            if (TARGET.getState() != TargetState.HALTED) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.wait_halt: Could not halt target", s);
            }
        }

        /**
         * Behaves like halt(long maxWaitMilliseconds) except sending a halt request to the target
         *
         * @param maxWaitMilliseconds Maximal waiting time
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of a timeout or if the target state has not changed to halted.
         */
        @Deprecated
        public void wait_halt(long maxWaitMilliseconds) throws IOException {
            if (maxWaitMilliseconds < 0) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.wait_halt: Cannot wait for a negative time");
            }
            String s = INTERFACE.transmit("ocd_wait_halt " + maxWaitMilliseconds);
            if (TARGET.getState() != TargetState.HALTED) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.wait_halt: Could not halt target", s);
            }
        }

        /**
         * Resume the target at its current code position. OpenOCDWrapper will wait 5 seconds
         * for the target to resume.
         *
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown if the target has not changed its state to running
         */
        public void resume() throws IOException {
            String s = INTERFACE.transmit("ocd_resume");
            if (TARGET.getState() != TargetState.RUNNING) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.resume: Target is not running", s);
            }
        }

        /**
         * Resume the target at the given address. OpenOCDWrapper will wait 5 seconds
         * for the target to resume.
         *
         * @param address Address at which execution should be resumed
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an illegal address or if the target hast not changed its state to running
         */
        public void resume(long address) throws IOException {
            if (address < 0) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.resume: Cannot resume at address " + address);
            }
            String s = INTERFACE.transmit("ocd_resume " + address);
            if (TARGET.getState() != TargetState.RUNNING) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.resume: Target is not running", s);
            }
        }

        /**
         * Single-step the target at its current code position.
         *
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown if the target is not halted
         */
        public void step() throws IOException {
            String s = INTERFACE.transmit("ocd_step");
            if (s.contains("target not halted")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.step: Target not halted", s);
            }
        }

        /**
         * Single-step the target at the address specified
         *
         * @param address Address at which a single-step should be taken
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown if the target is not halted
         */
        public void step(long address) throws IOException {
            String s = INTERFACE.transmit("ocd_step " + address);
            if (s.contains("target not halted")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.step: Target not halted", s);
            }
        }

        /**
         * Perform as hard a reset as possible, using SRST. All defined targets will be
         * reset, and target events will fire during the reset sequence.
         *
         * @throws IOException Thrown in case of a communication error
         */
        public void reset() throws IOException {
            INTERFACE.transmit("ocd_reset");
        }

        /**
         * Perform as hard a reset as possible, using SRST. All defined targets will be
         * reset, and target events will fire during the reset sequence. After the reset
         * the target is set to run mode.
         *
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown if the target has not changed to the run state
         */
        public void reset_run() throws IOException {
            String s = INTERFACE.transmit("ocd_reset run");
            if (TARGET.getState() != TargetState.RUNNING) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.reset_run: Target is not running", s);
            }
        }

        /**
         * Perform as hard a reset as possible, using SRST. All defined targets will be
         * reset, and target events will fire during the reset sequence. After the reset
         * the target is halted.
         *
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown if the target has not halted
         */
        public void reset_halt() throws IOException {
            String s = INTERFACE.transmit("ocd_reset halt");
            if (TARGET.getState() != TargetState.HALTED) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.reset_halt: Could not run command correctly", s);
            }
        }

        /**
         * Perform as hard a reset as possible, using SRST. All defined targets will be
         * reset, and target events will fire during the reset sequence. After the reset
         * the reset-init script will be executed. Calling this procedure without a specified
         * reset-init script may result in undefined behaviour.
         *
         * @throws IOException Thrown in case of a communication error
         */
        public void reset_init() throws IOException {
            INTERFACE.transmit("ocd_reset init");
        }

        /**
         * Requesting target halt and executing a soft reset. This is often used when a target
         * cannot be reset and halted. The target, after reset is released begins to execute code.
         * OpenOCDWrapper attempts to stop the CPU and then sets the program counter back to the reset
         * vector. Unfortunately the code that was executed may have left the hardware in an
         * unknown state.
         *
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown if the target has not changed to the halt state
         */
        @Deprecated
        public void soft_reset_halt() throws IOException {
            String s = INTERFACE.transmit("ocd_soft_reset_halt");
            if (TARGET.getState() != TargetState.HALTED) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.soft_reset_halt: Could not run command correctly", s);
            }
        }

        /**
         * Display contents at the specified address as a 32-bit word
         *
         * @param address       If the target has a MMU which is present and active, address
         *                      is interpreted as a virtual address. Otherwise, or if phys is true,
         *                      address is interpreted as a physical address.
         * @param numberOfWords Specifies the numer of units that should be fetched
         * @param phys          Forces the address to be interpreted as a physical address. This parameter is
         *                      only relevant to targets which have a MMU present and active.
         * @return Returns the data from the specified memory location
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of a parsing error
         */
        public long[] mdw(long address, boolean phys, int numberOfWords) throws IOException {
            if (numberOfWords < 1) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.mdw: Cannot fetch less than 1 word");
            }
            String s = INTERFACE.transmit("ocd_mdw " + (phys ? "phys " : "") + address + " " + numberOfWords);
            return Arrays.stream(s
                    .replaceAll("[^a-zA-z0-9: ]", "")
                    .split(" "))
                    .filter(e -> !e.contains(":"))
                    .map(e -> "0x" + e)
                    .filter(SYS::isNumber)
                    .mapToLong(SYS::getLong).toArray();
        }

        /**
         * Display contents at the specified address as a 16-bit halfword
         *
         * @param address       If the target has a MMU which is present and active, address
         *                      is interpreted as a virtual address. Otherwise, or if phys is true,
         *                      address is interpreted as a physical address.
         * @param numberOfWords Specifies the numer of units that should be fetched
         * @param phys          Forces the address to be interpreted as a physical address. This parameter is
         *                      only relevant to targets which have a MMU present and active.
         * @return Returns the data from the specified memory location
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of a parsing error
         */
        public int[] mdh(long address, boolean phys, int numberOfWords) throws IOException {
            if (numberOfWords < 1) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.mdh: Cannot fetch less than 1 halfword");
            }
            String s = INTERFACE.transmit("ocd_mdh " + (phys ? "phys " : "") + address + " " + numberOfWords);

            return Arrays.stream(s
                    .replaceAll("[^a-zA-z0-9: ]", "")
                    .split(" "))
                    .filter(e -> !e.contains(":"))
                    .map(e -> "0x" + e)
                    .filter(SYS::isNumber)
                    .mapToInt(SYS::getInt).toArray();
        }

        /**
         * Display contents at the specified address as a 8-bit byte
         *
         * @param address       If the target has a MMU which is present and active, address
         *                      is interpreted as a virtual address. Otherwise, or if phys is true,
         *                      address is interpreted as a physical address.
         * @param numberOfWords Specifies the numer of units that should be fetched
         * @param phys          Forces the address to be interpreted as a physical address. This parameter is
         *                      only relevant to targets which have a MMU present and active.
         * @return Returns the data from the specified memory location
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of a parsing error
         */
        public int[] mdb(long address, boolean phys, int numberOfWords) throws IOException {
            if (numberOfWords < 1) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.mdb: Cannot fetch less than 1 byte");
            }
            String s = INTERFACE.transmit("ocd_mdb " + (phys ? "phys " : "") + address + " " + numberOfWords);

            return Arrays.stream(s
                    .replaceAll("[^a-zA-z0-9: ]", "")
                    .split(" "))
                    .filter(e -> !e.contains(":"))
                    .map(e -> "0x" + e)
                    .filter(SYS::isNumber)
                    .mapToInt(SYS::getInt).toArray();
        }

        /**
         * Writes the specified 32-bit word to the given address. When the current target
         * has an MMU which is present and active, address is interpreted as a virtual
         * address. Otherwise, or if phys is set to true, address is interpreted as a physical
         * address.
         *
         * @param address Address to which the data should be written
         * @param phys    If true, address is interpreted as a physical address. Otherwise (and if the
         *                target has an active MMU), address is interpreted as a virtual address.
         * @param data    32-bit dataword which should be written
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an unspecified error
         */
        public void mww(long address, boolean phys, int data) throws IOException {
            String s = INTERFACE.transmit("ocd_mww " + (phys ? "phys " : "") + address + " " + data);
            if (!s.isEmpty()) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.mww: Error", s);
            }
        }

        /**
         * Writes the specified 16-bit halfword to the given address. When the current target
         * has an MMU which is present and active, address is interpreted as a virtual
         * address. Otherwise, or if phys is set to true, address is interpreted as a physical
         * address.
         *
         * @param address Address to which the data should be written
         * @param phys    If true, address is interpreted as a physical address. Otherwise (and if the
         *                target has an active MMU), address is interpreted as a virtual address.
         * @param data    16-bit halfword which should be written
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an unspecified error
         */
        public void mwh(long address, boolean phys, short data) throws IOException {
            String s = INTERFACE.transmit("ocd_mwh " + (phys ? "phys " : "") + address + " " + data);
            if (!s.isEmpty()) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.mwh: Error", s);
            }
        }

        /**
         * Writes the specified 8-bit byte to the given address. When the current target
         * has an MMU which is present and active, address is interpreted as a virtual
         * address. Otherwise, or if phys is set to true, address is interpreted as a physical
         * address.
         *
         * @param address Address to which the data should be written
         * @param phys    If true, address is interpreted as a physical address. Otherwise (and if the
         *                target has an active MMU), address is interpreted as a virtual address.
         * @param data    8-bit byte which should be written
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an unspecified error
         */
        public void mwb(long address, boolean phys, byte data) throws IOException {
            String s = INTERFACE.transmit("ocd_mwb " + (phys ? "phys " : "") + address + " " + data);
            if (!s.isEmpty()) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.mwb: Error", s);
            }
        }

        /**
         * Lists all active breakpoints
         *
         * @return All active breakpoints
         * @throws IOException Thrown in case of a communication error
         */
        public Collection<Breakpoint> bp() throws IOException {
            Collection<Breakpoint> list = new LinkedList<>();
            String[] breakpoints = INTERFACE.transmit("ocd_bp").split(System.lineSeparator());
            for (String b : breakpoints) {
                b = b.replaceAll(":", "");
                String[] split = b.split(" ");
                if (split.length != 4) {
                    continue;
                }
                Breakpoint newBp = new Breakpoint();
                newBp.setDescription(split[0]);
                newBp.setAddress(SYS.getLong(split[1]));
                newBp.setSize(SYS.getInt(split[2]));
                newBp.setNumber(SYS.getInt(split[3]));
                list.add(newBp);
            }
            return list;
        }

        /**
         * Sets a breakpoint on code execution at the specified address for the specified length.
         * This is a software breakpoint, unless isHardwareBP is true in which case it will be
         * a hardware breakpoint.
         *
         * @param address      Memory location at which the breakpoint should be set
         * @param size         Length of the breakpoint (in bytes)
         * @param isHardwareBP If true, the new breakpoint will be a hardware breakpoint, otherwise a
         *                     software breakpoint
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown if the breakpoint cannot be set
         */
        public void bp(long address, long size, boolean isHardwareBP) throws IOException {
            String s;
            if (isHardwareBP) {
                s = INTERFACE.transmit("ocd_bp " + address + " " + size + " hw");
            } else {
                s = INTERFACE.transmit("ocd_bp " + address + " " + size);
            }
            if (!s.contains("breakpoint set at")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.bp: Could not set breakpoint", s);
            }
        }

        /**
         * Removes the breakpoint from the specified memory location
         *
         * @param address Address of the breakpoint
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown if the breakpoint cannot be removed
         *                          (e.g. if there is no breakpoint at the specified memory location)
         */
        public void rbp(long address) throws IOException {
            String s = INTERFACE.transmit("ocd_rbp " + address);
            if (s.contains("no breakpoint at address")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.rbp: " + s, s);
            }
        }

        /**
         * Removes the data watchpoint from the specified memory location
         *
         * @param address Address of the watchpoint
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown if no data watchpoint was removed from the specified address
         */
        public void rwp(long address) throws IOException {
            String s = INTERFACE.transmit("ocd_rwp " + address);
            if (s.contains("no watchpoint at address")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.rwp: " + s, s);
            }
        }

        /**
         * Returns a list of all active data watchpoints
         *
         * @return All active watchpoints
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of a parsing error
         */
        public Collection<Watchpoint> wp() throws IOException {
            Collection<Watchpoint> list = new LinkedList<>();
            String s = INTERFACE.transmit("ocd_sp");
            String[] wps = s.split(System.lineSeparator());
            for (String wp : wps) {
                wp.replaceAll("[^0-9 ]", "");
                String split[] = wp.split(" ");
                Watchpoint newWp = new Watchpoint();
                newWp.setAddress(SYS.getLong(split[0]));
                newWp.setSize(SYS.getLong(split[1]));
                switch (SYS.getInt(split[2])) {
                    case 0:
                        newWp.setType(Watchpoint.AccessType.READ);
                        break;
                    case 1:
                        newWp.setType(Watchpoint.AccessType.WRITE);
                        break;
                    case 2:
                        newWp.setType(Watchpoint.AccessType.ACCESS);
                        break;
                    default:
                        throw new OpenOCDException("OpenOCDWrapper.GENERAL.rwp: Could not parse mask", s);
                }
                newWp.setValue(SYS.getLong(split[3]));
                newWp.setMask(SYS.getLong(split[4]));
                list.add(newWp);
            }
            return list;
        }

        /**
         * Sets a data watchpoint on data at the specified address for a given length. The possible
         * types are access, read and write.
         *
         * @param address Address at which the watchpoint should be created
         * @param length  Length of the watchpoint in bytes
         * @param type    Type of the watchpoint
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an unspecified error
         */
        public void wp(long address, long length, Watchpoint.AccessType type) throws IOException {
            String cmd = "ocd_wp " + address + " " + length;
            switch (type) {
                case READ:
                    cmd += " r";
                    break;
                case WRITE:
                    cmd += " w";
                    break;
                default:
                    break;
            }
            String s = INTERFACE.transmit(cmd);
            if (!s.isEmpty()) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.wp: " + s, s);
            }
        }

        /**
         * Sets a data watchpoint on data at the specified address for a given length. The possible
         * types are access, read and write. The watchpoint is triggered according to the given data
         *
         * @param address Address at which the watchpoint should be created
         * @param length  Length of the watchpoint in bytes
         * @param type    Type of the watchpoint
         * @param data    Trigger data
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an unspecified error
         */
        public void wp(long address, long length, Watchpoint.AccessType type, Long data) throws IOException {
            String cmd = "ocd_wp " + address + " " + length;
            switch (type) {
                case READ:
                    cmd += " r";
                    break;
                case WRITE:
                    cmd += " w";
                    break;
                default:
                    break;
            }
            cmd += " " + Long.toUnsignedString(data, 16);
            String s = INTERFACE.transmit(cmd);
            if (!s.isEmpty()) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.wp: " + s, s);
            }
        }

        /**
         * Sets a data watchpoint on data at the specified address for a given length. The possible
         * types are access, read and write. The watchpoint is triggered according to the given data masked
         * with the given mask.
         *
         * @param address Address at which the watchpoint should be created
         * @param length  Length of the watchpoint in bytes
         * @param type    Type of the watchpoint
         * @param data    Trigger data
         * @param mask    Masks the trigger data
         * @throws IOException      Thrown in case of a communication error
         * @throws OpenOCDException Thrown in case of an unspecified error
         */
        public void wp(long address, long length, Watchpoint.AccessType type, Long data, Long mask) throws IOException {
            String cmd = "ocd_wp " + address + " " + length;
            switch (type) {
                case READ:
                    cmd += " r";
                    break;
                case WRITE:
                    cmd += " w";
                    break;
                default:
                    break;
            }
            cmd += " " + Long.toUnsignedString(data, 16);
            cmd += " " + Long.toUnsignedString(mask, 16);
            String s = INTERFACE.transmit(cmd);
            if (!s.isEmpty()) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.wp: " + s, s);
            }
        }

        public void testImage(String filename) throws IOException {
            String s = INTERFACE.transmit("ocd_test_image " + filename + " " + FLASH.getBaseAddress());
            if (s.contains("Error")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.testImage: Could not test image", s);
            }
        }

        public void verifyImage(String filename) throws IOException {
            String s = INTERFACE.transmit("ocd_verify_image " + filename + " " + FLASH.getBaseAddress());
            if (s.contains("Error")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.verifyImage: Could not verify image", s);
            }
        }

        public void verifyImageOnlyChecksum(String filename) throws IOException {
            String s = INTERFACE.transmit("ocd_verify_image_checksum " + filename + " " + FLASH.getBaseAddress());
            if (s.contains("Error")) {
                throw new OpenOCDException("OpenOCDWrapper.GENERAL.verifyImageChecksum: Could not verify image", s);
            }
        }

        /**
         * Retrieves the current version of the running openOCD instance
         *
         * @throws IOException Thrown in case of a communication error
         */
        public String version() throws IOException {
            return INTERFACE.transmit("version");
        }

        /**
         * Converts a virtual address to a physical address.
         *
         * @param virtualAddress Virtual address which should be converted
         * @return If a MMU is present, returns the virtual address, otherwise returns the passed argument
         * @throws IOException
         */
        public long virt2phys(long virtualAddress) throws IOException {
            return SYS.getLong(INTERFACE.transmit("ocd_virt2phys " + virtualAddress).replaceAll("[^0-9x]", ""));
        }

    }

    public class ArchitectureAndCoreCommands {
        public void cortex_m_maskisr_enable() throws IOException {
            INTERFACE.transmit("cortex_m maskisr on");
        }

        public void cortex_m_maskisr_disable() throws IOException {
            INTERFACE.transmit("cortex_m maskisr off");
        }

        public void cortex_m_maskisr_auto() throws IOException {
            INTERFACE.transmit("cortex_m maskisr auto");
        }

        public void cortex_m_vector_catch_all() throws IOException {
            INTERFACE.transmit("cortex_m vector_catch all");
        }

        public void cortex_m_vector_catch_none() throws IOException {
            INTERFACE.transmit("cortex_m vector_catch none");
        }

        public String getResetType() throws IOException {
            return INTERFACE.transmit("ocd_cortex_m reset_config").split(" ")[2];
        }

        /**
         * Set the reset mode
         *
         * @param type srst | sysresetreq | vectreset
         * @throws IOException
         */
        public void setResetType(String type) throws IOException {
            INTERFACE.transmit("ocd_cortex_m reset_config " + type);
        }

    }

    public class JtagCommands {
        /**
         * Get the number of recent flushes of the JTAG chain
         *
         * @return Number of flushes
         * @throws IOException Thrown in case of a communication error
         */
        public long getFlushCount() throws IOException {
            return SYS.getLong(INTERFACE.transmit("flush_count"));
        }
    }

    public OpenOCDWrapper(InterfaceWrapper INTERFACE) {
        this.INTERFACE = INTERFACE;
    }

    public void close() throws IOException {
        INTERFACE.close();
    }

    public InterfaceWrapper getInterface() {
        return INTERFACE;
    }
}
