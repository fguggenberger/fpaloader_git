package openocd;

import java.util.Collection;

public class FtdiGpioSignal {

    private String name;
    private int pinNumber;
    private int oePinNumber = -1;

    private PinState defaultPinState;
    private PinState defaultOeState = null;
    private SignalDirection defaultSignalDirection;

    private boolean isInvertedSignal = false;
    private boolean isInvertedOe = false;

    private SignalType signalType;
    private SignalDirection signalDirection;

    public PinState getDefaultOeState() {
        return defaultOeState;
    }

    public void setDefaultOeState(PinState defaultOeState) {
        this.defaultOeState = defaultOeState;
    }

    enum SignalDirection {
        IN, OUT, INOUT;
    }

    enum PinState {
        HIGH("1"), LOW("0"), HIZ("z");

        private String name;

        PinState(String state) {
            name = state;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    enum SignalType {
        INPUT,
        PUSH_PULL,
        OPEN_COLLECTOR,
        PUSH_PULL_SEPARATE_OE,
        DYNAMIC,
        JTAG;
    }

    public FtdiGpioSignal(String name,
                          int pinNumber,
                          SignalType signalType,
                          SignalDirection signalDirection,
                          SignalDirection defaultSignalDirection,
                          PinState defaultPinState,
                          boolean isInvertedSignal) {
        this.name = name;
        this.pinNumber = pinNumber;
        this.defaultPinState = defaultPinState;
        this.signalType = signalType;
        this.signalDirection = signalDirection;
        this.defaultSignalDirection = defaultSignalDirection;
        this.isInvertedSignal = isInvertedSignal;
    }

    public String getName() {
        return name;
    }

    public int getPinNumber() {
        return pinNumber;
    }

    public void setOePinNumber(int oePinNumber) {
        this.oePinNumber = oePinNumber;
    }

    public int getOePinNumber() {
        return oePinNumber;
    }


    public SignalType getSignalType() {
        return signalType;
    }

    public boolean isInvertedSignal() {
        return isInvertedSignal;
    }

    public boolean isInvertedOe() {
        return isInvertedOe;
    }

    public void setInvertedOe(boolean invertedOe) {
        isInvertedOe = invertedOe;
    }

    public PinState getDefaultPinState() {
        return defaultPinState;
    }

    public SignalDirection getDefaultSignalDirection() {
        return defaultSignalDirection;
    }

    public SignalDirection getSignalDirection() {
        return signalDirection;
    }

    public String getConfig() throws OpenOCDException {
        String config = name;

        final int pinMask = 1 << pinNumber;
        final int oePinMask = 1 << oePinNumber;

        switch (signalType) {
            case INPUT:
                config += isInvertedSignal ? " -ninput " : " -input ";
                config += pinMask;
                break;
            case PUSH_PULL:
                config += isInvertedSignal ? " -ndata " : " -data ";
                config += pinMask;
                break;
            case OPEN_COLLECTOR:
                config += isInvertedSignal ? " -noe " : " -oe ";
                config += pinMask;
                break;
            case PUSH_PULL_SEPARATE_OE:
                config += isInvertedOe ? " -noe " : " -oe ";
                config += oePinMask;
                config += isInvertedSignal ? " -ndata " : " -data ";
                config += pinMask;
                break;
            case DYNAMIC:
                config += isInvertedOe ? " -noe " : " -oe ";
                config += pinMask;
                config += isInvertedSignal ? " -ndata " : " -data ";
                config += pinMask;
                break;
            case JTAG:
                throw new OpenOCDException("FtdiGpioSignal.getConfig: Cannot create a configuration for a JTAG signal");
        }
        return config;
    }

    public static long generateDefaultDataMask(Collection<FtdiGpioSignal> signals) throws OpenOCDException {
        long dataMask = 0;
        for (FtdiGpioSignal signal : signals) {
            if (signal.getDefaultPinState() == PinState.HIGH) {
                dataMask |= 1 << signal.getPinNumber();
            }
            if (signal.getSignalType() == SignalType.PUSH_PULL_SEPARATE_OE && signal.getDefaultOeState() == PinState.HIGH) {
                dataMask |= 1 << signal.getOePinNumber();
            }
        }
        return dataMask;
    }

    public static long generateDefaultDirectionMask(Collection<FtdiGpioSignal> signals) throws OpenOCDException {
        long directionMask = 0;
        for (FtdiGpioSignal signal : signals) {
            if (signal.getDefaultSignalDirection() == SignalDirection.OUT) {
                directionMask |= 1 << signal.getPinNumber();
            }
            if (signal.getSignalType() == SignalType.PUSH_PULL_SEPARATE_OE) {
                directionMask |= 1 << signal.getOePinNumber();
            }
        }
        return directionMask;
    }
}

