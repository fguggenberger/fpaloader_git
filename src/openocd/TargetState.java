package openocd;

public enum TargetState {
    HALTED("halted"),
    RESET("reset"),
    RUNNING("running"),
    UNKNOWN("unknown"),
    DEBUG_RUNNING("debug-running");

    private String name;

    private TargetState(String name){
        this.name = name;
    }
    @Override
    public String toString(){
        return name;
    }
}
