package openocd;

public class ResetSignal {
    public enum ResetSignalType{
        NONE, SRST_ONLY, TRST_ONLY, TRST_AND_SRST;
    }
    public enum ResetSignalCombination{
        SEPARATE, SRST_PULLS_TRST, TRST_PULLS_SRST, COMBINED;
    }
    public enum ResetSignalGating{
        SRST_GATES_JTAG, SRST_NOGATE;
    }
    public enum ResetSignalConnectType{
        CONNECT_DEASSERT_SRST, CONNECT_ASSERT_SRST;
    }
    public enum ResetSignalTrstType{
        TRST_PUSH_PULL, TRST_OPEN_DRAIN;
    }
    public enum ResetSignalSrstType{
        SRST_PUSH_PULL, SRST_OPEN_DRAIN
    }
}
