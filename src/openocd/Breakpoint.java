package openocd;

public class Breakpoint {
    /**
     * Description of the breakpoint
     */
    private String description;
    /**
     * Memory address of the breakpoint
     */
    private long address;
    /**
     * Length of the breakpoint (in bytes)
     */
    private int size;
    /**
     * Number of the breakpoint. Ascending and unique for every target.
     */
    private int number;

    /**
     * Creates a new breakpoint, all attributes are set invalid.
     */
    public Breakpoint() {
        description = "";
        address = -1;
        size = 0;
        number = -1;
    }

    /**
     * Constructs a new breakpoint with the given arguments
     *
     * @param description Description of the breakpoint
     * @param address     Memory address of the breakpoint
     * @param size        Length of the breakpoint
     * @param number      Number of the breakpoint (ascending and unique for each target)
     */
    public Breakpoint(String description, long address, int size, int number) {
        this.description = description;
        this.address = address;
        this.size = size;
        this.number = number;
    }

    public long getAddress() {
        return address;
    }

    public void setAddress(long address) {
        this.address = address;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Breakpoint) {
            Breakpoint b = (Breakpoint) o;
            if (b.getSize() == size &&
                    b.getNumber() == number &&
                    b.getDescription().equals(description) &&
                    b.getAddress() == address) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return description + ": " + address + ", " + size + ", " + number;
    }
}


