package openocd;

public class Watchpoint {

    /**
     * Memory address of the Watchpoint
     */
    private long address;

    /**
     * Bitmask for masking the relevant bits
     */
    private long mask;
    /**
     * Type of the Watchpoint. Can be access/read/write
     * (see {@link AccessType})
     */
    private AccessType type;
    /**
     * Data at the specified memory location.
     */
    private long value;
    /**
     * Specified length of the Watchpoint
     */
    private long size;

    public long getAddress() {
        return address;
    }

    public void setAddress(long address) {
        this.address = address;
    }

    public long getMask() {
        return mask;
    }

    public void setMask(long mask) {
        this.mask = mask;
    }

    public AccessType getType() {
        return type;
    }

    public void setType(AccessType type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }


    /**
     * Enumerator for the possible types of Watchpoints
     */
    enum AccessType {
        READ, WRITE, ACCESS;
    }


    public Watchpoint() {
        super();
    }

    public Watchpoint(long address, long mask, long value, long size, AccessType type) {
        this.address = address;
        this.mask = mask;
        this.value = value;
        this.size = size;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Watchpoint) {
            Watchpoint w = (Watchpoint) o;
            if (w.getAddress() == address
                    && w.getMask() == mask
                    && w.getType() == type
                    && w.getSize() == size) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "address: " + address +
                ", len: " + size +
                ", type: " + type.name() +
                ", value: " + value +
                ", mask: " + mask;
    }

}
