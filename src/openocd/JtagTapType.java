package openocd;

public enum JtagTapType {
    BS,             // Boundary scan if this is a separate Tap
    CPU,            // The main CPU of the chip
    ETB,            // For an embedded trace buffer
    FLASH,          // If the chip has a flash tap
    JRC,            // For JTAG route controller
    TAP,            // Should be used only for FPGA- or CPLD-like devices with a single tap
    UNKNOWN0;       // For unknown taps
}
