package openocd;

import java.io.IOException;

public class OpenOCDException extends IOException {
    private String responseMessage;

    /**
     * Constructs a new OpenOCDException with the given arguments
     * @param msg               Text of the exception
     * @param responseMessage   Return value of the openOCD method call
     */
    public OpenOCDException(String msg, String responseMessage) {
        super(msg);
        this.responseMessage = responseMessage;
    }

    public OpenOCDException(String msg){
        super(msg);
        responseMessage = "";
    }

    /**
     * @return Return value of the openOCD function call
     */
    public String getResponseMessage() {
        return responseMessage;
    }
}

