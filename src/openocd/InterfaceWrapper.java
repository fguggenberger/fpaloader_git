package openocd;

import java.io.*;
import java.net.Socket;

public class InterfaceWrapper implements Closeable {
    private Socket socket = null;
    private DataOutputStream out = null;
    private BufferedReader in = null;
    public InterfaceWrapper(String serverAddress, int TclServerPort) throws IOException {
        if (socket != null || in != null || out != null) {
            throw new OpenOCDException("OpenOCDWrapper.connect: Connection was not disconnected before");
        }
        try {
            socket = new Socket(serverAddress, TclServerPort);
            socket.setReceiveBufferSize(512);
            socket.setSendBufferSize(512);
            socket.setReuseAddress(true);
            socket.setTrafficClass(0x04);

            out = new DataOutputStream(socket.getOutputStream());
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    public void close() throws IOException {
        in.close();
        out.close();
        socket.close();
    }
    /**
     * Executes the given command at the connected host.
     *
     * @param command Command to be executed
     * @return Return message of the openOCD instance
     * @throws IOException Thrown in case of a communication error
     */
    String transmit(String command) throws IOException {
        final char COMMAND_TOKEN = 0x1a;

        byte[] message = (command + COMMAND_TOKEN).getBytes("UTF-8");
        String answer;
        do {
            out.write(message);
            out.flush();
            StringBuilder s = new StringBuilder();
            while (true) {
                int i = in.read();
                if (i == -1)
                    break;
                if (i == COMMAND_TOKEN) {
                    break;
                }
                s.append((char) i);
            }
            answer = s.toString().replaceAll("[" + Character.toString((char) COMMAND_TOKEN) + "]", "");
        } while (answer.contains("error during write") || answer.contains("error during read"));
        return answer;
    }
}

