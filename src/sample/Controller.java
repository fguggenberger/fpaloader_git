package sample;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.*;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    final Logger logger = LoggerFactory.getLogger(Controller.class);

    @FXML
    HBox menuHBox;
    @FXML
    Label progressHeader;
    @FXML
    VBox progressVBox;
    @FXML
    Label loadingStatus;
    @FXML
    ProgressBar progressBar;
    @FXML
    Button btnPreparation;
    @FXML
    Button btnFirmware;
    @FXML
    Button btnCalibrate;
    @FXML
    HBox refreshHBox;

    FPA fpa;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        init();
    }

    private void init() {
        loadingStatus.setText("Starting initialization...");
        Thread t = new Thread(()->{
            try {
                Thread.sleep(2000);
                onLoaded();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t.start();
    }

    private void onLoaded(){
        Task<FPA> initTask = FPA.getInitTask();
        loadingStatus.textProperty().bind(initTask.messageProperty());
        progressBar.progressProperty().bind(initTask.progressProperty());

        initTask.setOnSucceeded(e -> {
            progressBar.progressProperty().unbind();
            loadingStatus.textProperty().unbind();

            fpa= initTask.getValue();
            Platform.runLater(()->{
                menuHBox.setVisible(true);
                btnPreparation.setDisable(!fpa.isPreparationPossible());
                btnFirmware.setDisable(!fpa.isFirmwareUpdatePossible());
                btnCalibrate.setDisable(!fpa.isCalibrationPossible());
                progressVBox.setVisible(false);
            });
            if(!fpa.isPreparationPossible() && !fpa.isFirmwareUpdatePossible() && !fpa.isCalibrationPossible()){
                Platform.runLater(()->{
                    refreshHBox.setVisible(true);
                });
            }
        });

        final Thread th = new Thread(initTask);
        th.setUncaughtExceptionHandler((t, e) -> logger.error(e.getMessage()));
        th.start();
    }

    @FXML
    private void onPreparation(final ActionEvent event) {
        String msg = "Flashing FTDI...";
        logger.info(msg);
        progressHeader.setText(msg);
        menuHBox.setVisible(false);
        progressVBox.setVisible(true);

        Task<Boolean> setSerialTask = FTDI.getSetNewSerialTask();
        loadingStatus.textProperty().bind(setSerialTask.messageProperty());
        progressBar.progressProperty().bind(setSerialTask.progressProperty());

        setSerialTask.setOnSucceeded(e -> {
            progressBar.progressProperty().unbind();
            loadingStatus.textProperty().unbind();
            menuHBox.setVisible(true);
            progressVBox.setVisible(false);

            if(setSerialTask.getValue()) {
                logger.info("FTDI serial number was successfully set!");
                init();
            }else{
                logger.info("Failed to set FTDI serial!");
            }
        });

        final Thread th = new Thread(setSerialTask);
        th.setUncaughtExceptionHandler((t, e) -> logger.error(e.getMessage()));
        th.start();
    }

    @FXML
    private void onFlashFirmware(final ActionEvent event) {
        String msg = "Flashing firmware...";
        logger.info(msg);
        progressHeader.setText(msg);
        menuHBox.setVisible(false);
        progressVBox.setVisible(true);

        Task<Boolean> flashFirmwareTask = FPA.getFlashFirmwareTask();
        loadingStatus.textProperty().bind(flashFirmwareTask.messageProperty());
        progressBar.progressProperty().bind(flashFirmwareTask.progressProperty());

        flashFirmwareTask.setOnSucceeded(e -> {
            progressBar.progressProperty().unbind();
            loadingStatus.textProperty().unbind();
            if(flashFirmwareTask.getValue()){
                logger.info("Firmware update succeeded!");
                menuHBox.setVisible(true);
                progressVBox.setVisible(false);
                init();
            }
        });

        final Thread th = new Thread(flashFirmwareTask);
        th.setUncaughtExceptionHandler((t, e) -> logger.error("Faild to flash firmware", e.getMessage()));
        th.start();
    }

    @FXML
    private void onCalibrating(final ActionEvent event) {
        String msg = "Calibrating...";
        logger.info(msg);
        progressHeader.setText(msg);
        menuHBox.setVisible(false);
        progressVBox.setVisible(true);

        Task<Boolean> calibrationTask = FPA.getCalibrationTask();
        loadingStatus.textProperty().bind(calibrationTask.messageProperty());
        progressBar.progressProperty().bind(calibrationTask.progressProperty());

        calibrationTask.setOnSucceeded(e -> {
            progressBar.progressProperty().unbind();
            loadingStatus.textProperty().unbind();
            menuHBox.setVisible(true);
            progressVBox.setVisible(false);
        });

        final Thread th = new Thread(calibrationTask);
        th.setUncaughtExceptionHandler((t, e) -> logger.error(e.getMessage()));
        th.start();

    }

    @FXML
    private void onRefresh(final ActionEvent event) {
        refreshHBox.setVisible(false);
        menuHBox.setVisible(false);
        progressVBox.setVisible(true);
        init();
    }
}
