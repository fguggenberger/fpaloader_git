# FPA LOADER
> FPA Loader (Prepares an standard actuatorboard into an usable FPA).

Searchs for a connected actuatorboard. Either a UAB or a FPA. If it finds one it flashes a new serial number to the FTDI chip
(in case if it is a UAB) or it allows to flash the firmware or do calibration on the FPA.



## Installation

Linux:

```sh
wget -O  fpa-post-install.sh "http://10.156.4.100/fpa/fpa-post-install.sh" --no-check-certificate
sudo chmod +x fpa-post-install.sh
sudo ./ fpa-post-install.sh
```

Windows:

```sh
This software does not work under Windows!
```

## Development setup
TBD

## Release History
* 0.0.1
    * Work in progress

## Meta

Florian Guggenberger – florian.guggenberger@ametek.com

Distributed under the XYZ license. See ``LICENSE`` for more information.

[https://bitbucket.org/fguggenberger/fpaloader_git/](https://github.com/fguggenberger/)

## Contributing

1. Fork it (<https://github.com/yourname/yourproject/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://github.com/yourname/yourproject/wiki
